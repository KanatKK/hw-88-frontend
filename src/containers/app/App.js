import React from 'react';
import './App.css';
import {Route, Switch, BrowserRouter} from 'react-router-dom';
import MainPage from "../mainPage/mainPage";
import Login from "../../components/Registration/Login";
import SignUp from "../../components/Registration/SignUp";
import Post from "../../components/Posts/Post";
import AddPost from "../../components/Posts/AddPost";

const App = () => {
  return (
      <BrowserRouter>
          <Switch>
              <Route path="/" exact component={MainPage}/>
              <Route path="/login" exact component={Login}/>
              <Route path="/register" exact component={SignUp}/>
              <Route path="/post/:id" exact component={Post}/>
              <Route path={"/addNewPost"} exact component={AddPost}/>
          </Switch>
      </BrowserRouter>
  );
};

export default App;
