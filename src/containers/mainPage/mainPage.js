import React, {useEffect} from 'react';
import RegBlock from "../../components/Registration/RegBlock";
import {useDispatch, useSelector} from "react-redux";
import {fetchPosts} from "../../store/actions";
import PostList from "../../components/Posts/PostList";
import Spinner from "../../components/Spinner/Spinner";

const MainPage = () => {
    const dispatch = useDispatch();
    const posts = useSelector(state => state.posts.posts);

    useEffect(() => {
            dispatch(fetchPosts());
    }, [dispatch]);

    useEffect(() => {
        const interval = setInterval(() => {
            dispatch(fetchPosts());
        }, 2000);
        return () => clearInterval(interval);
    }, [dispatch]);

    if (posts !== null) {
        const postsList = posts.map((post, index) => {
            return (
                <PostList
                    key={index} id={post._id} title={post.title}
                    image={post.image} time={post.time} author={post.author}
                />
            );
        });

        return (
            <div className="container">
                <header><h2>Forum</h2><RegBlock/></header>
                <div className="posts">
                    {postsList}
                </div>
            </div>
        );
    } else {
        return (
            <div className="container">
                <header><h2>Forum</h2><RegBlock/></header>
                <div className="posts">
                    <Spinner/>
                </div>
            </div>
        );
    }
};

export default MainPage;


