import {GET_POST, GET_POSTS} from "../actionTypes";

const initialState = {
    posts: null,
    post: null,
};

const getPostsReducer = (state = initialState, action) => {
    switch (action.type) {
        case GET_POST:
            return {...state, post: action.value};
        case GET_POSTS:
            return {...state, posts: action.value};
        default:
            return state;
    }
};

export default getPostsReducer;