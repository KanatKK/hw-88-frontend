import {ADD_TIME, ADD_DESCRIPTION, ADD_IMAGE, ADD_IMAGE_NAME, ADD_TEXT} from "../actionTypes";

const initialState = {
    title: "",
    description: "",
    image: null,
    imageName: "Choose an image.",
    time: "",
};

const addPostsReducer = (state = initialState, action) => {
    switch (action.type) {
        case ADD_IMAGE:
            return {...state, image: action.value};
        case ADD_IMAGE_NAME:
            return {...state, imageName: action.value};
        case ADD_DESCRIPTION:
            return  {...state, description: action.value};
        case ADD_TIME:
            return {...state, time: action.value};
        case ADD_TEXT:
            return {...state, title: action.value};
        default:
            return state;
    }
};

export default addPostsReducer;