import {GET_COMMENT_TXT, GET_POST_ID} from "../actionTypes";

const initialState = {
    text: "",
    postId: "",
};

const addCommentsReducer = (state = initialState, action) => {
    switch (action.type) {
        case GET_COMMENT_TXT:
            return {...state, text: action.value};
        case GET_POST_ID:
            return {...state, postId: action.value};
        default:
            return state;
    }
};

export default addCommentsReducer;