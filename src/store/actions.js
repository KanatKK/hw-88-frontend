import axios from "axios";
import {
    ADD_TIME, ADD_DESCRIPTION,
    ADD_IMAGE, ADD_IMAGE_NAME,
    ADD_PASSWORD, ADD_TEXT,
    ADD_USERNAME,
    GET_COMMENT_TXT,
    GET_COMMENTS,
    GET_POST,
    GET_POST_ID,
    GET_POSTS,
    GET_USER
} from "./actionTypes";

export const addUserName = value => {
    return {type: ADD_USERNAME, value};
};
export const addPassword = value => {
    return {type: ADD_PASSWORD, value};
};

export const fetchUser = value => {
    return {type: GET_USER, value};
};

export const getPostsList = value => {
    return {type: GET_POSTS, value};
};
export const getPost = value => {
    return {type: GET_POST, value};
};

export const getComments = value => {
    return {type: GET_COMMENTS, value};
};
export const getPostId = value => {
    return {type: GET_POST_ID, value}
}
export const getCommentTxt = value => {
    return {type: GET_COMMENT_TXT, value};
};

export const addImage = value => {
    return {type: ADD_IMAGE, value};
};
export const addTime = value => {
    return {type: ADD_TIME, value};
};
export const addImageName = value => {
    return {type: ADD_IMAGE_NAME, value};
};
export const addDescription = value => {
    return {type: ADD_DESCRIPTION, value};
};
export const addText = value => {
    return {type: ADD_TEXT, value};
};

export const fetchComments = (postId) => {
    return async dispatch => {
        try {
            const response = await axios.get('http://localhost:8000/comments/' + postId);
            dispatch(getComments(response.data));
        } catch (e) {
            console.log(e);
        }
    };
};

export const fetchPosts = () => {
    return async dispatch => {
        try {
            const response = await axios.get('http://localhost:8000/posts');
            dispatch(getPostsList(response.data));
        } catch (e) {
            console.log(e);
        }
    };
};

export const fetchPost = (post) => {
    return async dispatch => {
        try {
            const response = await axios.get('http://localhost:8000/posts/' + post);
            dispatch(getPost(response.data));
        } catch (e) {
            console.log(e);
        }
    };
};

export const registerUser = (data) => {
    return async dispatch => {
        try {
            const response = await axios.post('http://localhost:8000/users/sessions', data);
            dispatch(fetchUser(response.data));
        } catch (e) {
            if (e.response && e.response.data) {
                dispatch(fetchUser(e.response.data));
            }
        }
    };
};

export const createUser = (data) => {
    return async dispatch => {
        try {
            const response = await axios.post('http://localhost:8000/users', data);
            dispatch(fetchUser(response.data))
        } catch (e) {
            if (e.response && e.response.data) {
                dispatch(fetchUser(e.response.data));
            }
        }
    };
};

export const logOutUser = () => {
    return async (dispatch, getState) => {
        const token = getState().user.user.token;
        const headers = {"Authorization": token};
        await axios.delete('http://localhost:8000/users/sessions', {headers});
        dispatch(fetchUser(null));
    };
};

export const addComment = async (header, comment) => {
    try {
        await axios.post('http://localhost:8000/comments', comment, {headers: header});
    } catch (e) {
        console.log(e);
    }
};

export const addNewPost = async (header, post) => {
    try {
        await axios.post('http://localhost:8000/posts', post, {headers: header});
    } catch (e) {
        console.log(e);
    }
};