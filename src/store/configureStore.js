import {applyMiddleware, combineReducers, createStore} from "redux";
import {loadFromLocalStorage, saveToLocalStorage} from "./localStorage";
import thunkMiddleware from "redux-thunk";
import getUserReducer from "./reducers/getUserReducer";
import addUserReducer from "./reducers/addUserReducer";
import getPostsReducer from "./reducers/getPostsReducer";
import addPostsReducer from "./reducers/addPostReducer";
import getCommentsReducer from "./reducers/getCommentsReducer";
import addCommentsReducer from "./reducers/addCommentsReducer";

const rootReducers = combineReducers({
    user: getUserReducer,
    addUser: addUserReducer,
    posts: getPostsReducer,
    addPost: addPostsReducer,
    comments: getCommentsReducer,
    addComment: addCommentsReducer,
});

const persistedState = loadFromLocalStorage();

const store = createStore(rootReducers, persistedState, applyMiddleware(thunkMiddleware));

store.subscribe(() => {
    saveToLocalStorage({
        user: {
            user: store.getState().user.user
        }
    });
});

export default store;