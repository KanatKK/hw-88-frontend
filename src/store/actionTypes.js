export const ADD_USERNAME = "ADD_USERNAME";
export const ADD_PASSWORD = "ADD_PASSWORD";
export const GET_USER = "GET_USER";

export const GET_POSTS = "GET_POSTS";
export const GET_POST = "GET_POST";

export const GET_COMMENTS = "GET_COMMENTS";
export const GET_POST_ID = "GET_POST_ID";
export const GET_COMMENT_TXT = "GET_COMMENT_TXT";

export const ADD_TEXT = 'ADD_TEXT';
export const ADD_DESCRIPTION = 'ADD_DESCRIPTION';
export const ADD_IMAGE = 'ADD_IMAGE';
export const ADD_TIME = 'ADD_TIME';
export const ADD_IMAGE_NAME = 'ADD_IMAGE_NAME';