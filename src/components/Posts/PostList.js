import React from 'react';
import {NavLink} from "react-router-dom";
import Moment from 'react-moment';

const PostList = props => {
    const postImage = () => {
        if (props.image !== "null") {
            return (
                <img src={'http://localhost:8000/uploads/' + props.image} alt=""/>
            );
        } else {
            return (
                <div className="img"/>
            )
        }
    };

    return (

        <div className="post">
            <div className="postImage">
                {postImage()}
            </div>
            <div className="postInfo">
                <p className="postAuthor">At <Moment>{props.time}</Moment> by {props.author.name}</p>
                <p>
                    <NavLink
                        style={{
                            color: "#0000FF",
                            textDecoration: 'underline',
                            cursor: 'pointer'
                        }}
                        to={"/post/" + props.id}
                    >
                        {props.title}
                    </NavLink>
                </p>
            </div>
        </div>
    );
};

export default PostList;