import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {addComment, fetchComments, fetchPost, getCommentTxt, getPostId} from "../../store/actions";
import RegBlock from "../Registration/RegBlock";
import Spinner from "../Spinner/Spinner";

const Post = props => {
    const dispatch = useDispatch();
    const post = useSelector(state => state.posts.post);
    const user = useSelector(state => state.user.user);
    const comments = useSelector(state => state.comments.comments);
    const commentTxt = useSelector(state => state.addComment.text);
    const commentData = useSelector(state => state.addComment);

    useEffect(() => {
        dispatch(fetchPost(props.match.params.id));
        dispatch(fetchComments(props.match.params.id));
        dispatch(getPostId(props.match.params.id));
    }, [dispatch, props.match.params.id]);

    useEffect(() => {
        const interval = setInterval(() => {
            dispatch(fetchComments(props.match.params.id));
        }, 2000);
        return () => clearInterval(interval);
    }, [dispatch, props.match.params.id]);

    const getCommentTxtHandler = (event) => {
        dispatch(getCommentTxt(event.target.value));
    };

    if (post !== null && comments !== null) {
        const addCommentHandler = async () => {
            const headers = {
                'Authentication': user.token,
            }
            await addComment(headers, commentData);
            dispatch(getCommentTxt(""));
        };

        const commentsList = comments.map((comment, index) => {
            return (
                <div className="comment" key={index}>
                    <h5 className="commentAuthor">{comment.author.name}</h5>
                    <p className="commentTxt">{comment.text}</p>
                </div>
            )
        });
        return (
            <div className="container">
                <header><h2>Forum</h2><RegBlock/></header>
                <div className="content">
                    {
                        post.image !== "null" &&
                        <div className="postImage" style={{width: 200, height: 200}}>
                            <img src={'http://localhost:8000/uploads/' + post.image} style={{width: 200, height: 200}} alt=""/>
                        </div>
                    }
                    <h1>{post.title}</h1>
                    {post.description && <p>{post.description}</p>}
                </div>
                <div className="comments">
                    <h3>Comments:</h3>
                    {
                        user &&
                        <>
                            <textarea
                                className="addComments" placeholder="Add your comment"
                                onChange={getCommentTxtHandler} value={commentTxt}
                            />
                            <button className="addCommentsBtn" onClick={addCommentHandler}>Add</button>
                        </>
                    }
                    <div className="allComments">
                        {commentsList}
                    </div>
                </div>
            </div>
        );
    } else{
        return <Spinner/>
    }
};

export default Post;