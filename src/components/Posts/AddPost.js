import React from 'react';
import {NavLink} from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";
import {addDescription, addImage, addImageName, addNewPost, addText, addTime, logOutUser} from "../../store/actions";

const AddPost = props => {
    const dispatch = useDispatch()
    const user = useSelector(state => state.user.user);
    const imageName = useSelector(state => state.addPost.imageName);
    const text = useSelector(state => state.addPost.title);
    const description = useSelector(state => state.addPost.description);
    const addPostReducer = useSelector(state => state.addPost);

    const addTextHandler = event => {
        dispatch(addText(event.target.value));
    };

    const addDescriptionHandler = event => {
        dispatch(addDescription(event.target.value));
        const dateNow = new Date();
        dispatch(addTime(dateNow.toISOString()));
    };

    const addImageHandler = event => {
        if (event.target.files[0] !== undefined) {
            const dateNow = new Date();
            dispatch(addTime(dateNow.toISOString()));
            dispatch(addImageName(event.target.files[0].name));
            dispatch(addImage(event.target.files[0]));
        } else {
            dispatch(addImage(null));
            dispatch(addImageName("Choose an image..."));
        }
    };

    const send = async event => {
        event.preventDefault();
        const formData = new FormData();
        Object.keys(addPostReducer).forEach(key => {
            if (key !== 'imageName') {
                formData.append(key, addPostReducer[key]);
            }
        });
        const headers = {
            'Authentication': user.token,
        }
        await addNewPost(headers, formData);
        dispatch(addText(""));
        dispatch(addDescription(""));
        dispatch(addImage(null));
        dispatch(addImageName("Choose an image."));
        dispatch(addTime(""));
        props.history.push("/");
    };

    const logOut = () => {
        dispatch(logOutUser());
    };
    return (
        <div className="container">
            <header>
                <h2>Add Post</h2>
                <span className="nav">
                <NavLink
                    style={{
                        color: 'black',
                        textDecoration: 'underline',
                        cursor: 'pointer',
                        marginRight: 5
                    }}
                    to="/"
                >
                    Main Page
                </NavLink>
                |
                <NavLink
                    onClick={logOut}
                    style={{
                        color: 'black',
                        textDecoration: 'underline',
                        cursor: 'pointer',
                        marginLeft: 5
                    }}
                    to="/"
                >
                    {user.username}
                </NavLink>
            </span>
            </header>
            <div className="content">
                <input
                    type="text" onChange={addTextHandler} value={text}
                    className="addPostField" placeholder="Text"
                />
                <input
                    type="text" onChange={addDescriptionHandler} value={description}
                    className="addPostField" placeholder="Description"
                />
                <label className="inputFileLabel" htmlFor="inputFile">{imageName}</label>
                <input
                    type="file" className="chooseImage" onChange={addImageHandler}
                    id="inputFile" accept=".jpg, .jpeg, .png"
                />
                <button onClick={send} className="addPostBtn">Add Post</button>
            </div>
        </div>
    );
};

export default AddPost;