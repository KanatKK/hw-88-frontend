import React from 'react';
import {useDispatch, useSelector} from "react-redux";
import {addPassword, addUserName, createUser} from "../../store/actions";
import {NavLink} from "react-router-dom";

const SignUp = props => {
    const dispatch = useDispatch();
    const user = useSelector(state => state.user.user);
    const username = useSelector(state => state.addUser.username);
    const password = useSelector(state => state.addUser.password);
    const userData = useSelector(state=> state.addUser);

    const toLogIn = () => {
        props.history.push("/login");
    };

    const addUserNameHandler = event => {
        dispatch(addUserName(event.target.value));
    };

    const addPasswordHandler = event => {
        dispatch(addPassword(event.target.value));
    };

    const createUserHandler = () => {
        dispatch(createUser(userData));
        dispatch(addUserName(""));
        dispatch(addPassword(""));
    };

    if (user !== null && user.username) {
        props.history.push("/");
    }

    return (
        <div className="container">
            <header>
                <h2>Registration</h2>
                <span className="nav">
                    <NavLink
                        style={{
                            color: 'black',
                            textDecoration: 'underline',
                            cursor: 'pointer'
                        }}
                        to="/"
                    >
                        MainPage
                    </NavLink>
                </span>
            </header>
            <div className="registration">
                <input
                    type="text" className="userName"
                    placeholder="User Name" value={username}
                    onChange={addUserNameHandler}
                />
                <input
                    type="text" className="password"
                    placeholder="Password" value={password}
                    onChange={addPasswordHandler}
                />
                <button type="button" onClick={createUserHandler} className="regButton">Login</button>
                {user && <span className="error">{user.error}</span>}
                <p onClick={toLogIn}>Already have an account? Login!</p>
            </div>
        </div>
    );
};

export default SignUp;